//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
//Date        : Tue Dec  8 15:04:09 2020
//Host        : chad-Tesseract running 64-bit Linux Mint 20
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (DDR3_0_addr,
    DDR3_0_ba,
    DDR3_0_cas_n,
    DDR3_0_ck_n,
    DDR3_0_ck_p,
    DDR3_0_cke,
    DDR3_0_cs_n,
    DDR3_0_dm,
    DDR3_0_dq,
    DDR3_0_dqs_n,
    DDR3_0_dqs_p,
    DDR3_0_odt,
    DDR3_0_ras_n,
    DDR3_0_reset_n,
    DDR3_0_we_n,
    mig_ref_clk_clk_n,
    mig_ref_clk_clk_p,
    mig_sys_clk_clk_n,
    mig_sys_clk_clk_p,
    pcie_7x_mgt_0_rxn,
    pcie_7x_mgt_0_rxp,
    pcie_7x_mgt_0_txn,
    pcie_7x_mgt_0_txp,
    pcie_endpt_reset,
    ref_pcie_clk_clk_n,
    ref_pcie_clk_clk_p,
    reset_pb,
    reset_rtl_0);
  output [15:0]DDR3_0_addr;
  output [2:0]DDR3_0_ba;
  output DDR3_0_cas_n;
  output [0:0]DDR3_0_ck_n;
  output [0:0]DDR3_0_ck_p;
  output [0:0]DDR3_0_cke;
  output [0:0]DDR3_0_cs_n;
  output [7:0]DDR3_0_dm;
  inout [63:0]DDR3_0_dq;
  inout [7:0]DDR3_0_dqs_n;
  inout [7:0]DDR3_0_dqs_p;
  output [0:0]DDR3_0_odt;
  output DDR3_0_ras_n;
  output DDR3_0_reset_n;
  output DDR3_0_we_n;
  input mig_ref_clk_clk_n;
  input mig_ref_clk_clk_p;
  input mig_sys_clk_clk_n;
  input mig_sys_clk_clk_p;
  input [3:0]pcie_7x_mgt_0_rxn;
  input [3:0]pcie_7x_mgt_0_rxp;
  output [3:0]pcie_7x_mgt_0_txn;
  output [3:0]pcie_7x_mgt_0_txp;
  output [0:0]pcie_endpt_reset;
  input [0:0]ref_pcie_clk_clk_n;
  input [0:0]ref_pcie_clk_clk_p;
  input reset_pb;
  input reset_rtl_0;

  wire [15:0]DDR3_0_addr;
  wire [2:0]DDR3_0_ba;
  wire DDR3_0_cas_n;
  wire [0:0]DDR3_0_ck_n;
  wire [0:0]DDR3_0_ck_p;
  wire [0:0]DDR3_0_cke;
  wire [0:0]DDR3_0_cs_n;
  wire [7:0]DDR3_0_dm;
  wire [63:0]DDR3_0_dq;
  wire [7:0]DDR3_0_dqs_n;
  wire [7:0]DDR3_0_dqs_p;
  wire [0:0]DDR3_0_odt;
  wire DDR3_0_ras_n;
  wire DDR3_0_reset_n;
  wire DDR3_0_we_n;
  wire mig_ref_clk_clk_n;
  wire mig_ref_clk_clk_p;
  wire mig_sys_clk_clk_n;
  wire mig_sys_clk_clk_p;
  wire [3:0]pcie_7x_mgt_0_rxn;
  wire [3:0]pcie_7x_mgt_0_rxp;
  wire [3:0]pcie_7x_mgt_0_txn;
  wire [3:0]pcie_7x_mgt_0_txp;
  wire [0:0]pcie_endpt_reset;
  wire [0:0]ref_pcie_clk_clk_n;
  wire [0:0]ref_pcie_clk_clk_p;
  wire reset_pb;
  wire reset_rtl_0;

  design_1 design_1_i
       (.DDR3_0_addr(DDR3_0_addr),
        .DDR3_0_ba(DDR3_0_ba),
        .DDR3_0_cas_n(DDR3_0_cas_n),
        .DDR3_0_ck_n(DDR3_0_ck_n),
        .DDR3_0_ck_p(DDR3_0_ck_p),
        .DDR3_0_cke(DDR3_0_cke),
        .DDR3_0_cs_n(DDR3_0_cs_n),
        .DDR3_0_dm(DDR3_0_dm),
        .DDR3_0_dq(DDR3_0_dq),
        .DDR3_0_dqs_n(DDR3_0_dqs_n),
        .DDR3_0_dqs_p(DDR3_0_dqs_p),
        .DDR3_0_odt(DDR3_0_odt),
        .DDR3_0_ras_n(DDR3_0_ras_n),
        .DDR3_0_reset_n(DDR3_0_reset_n),
        .DDR3_0_we_n(DDR3_0_we_n),
        .mig_ref_clk_clk_n(mig_ref_clk_clk_n),
        .mig_ref_clk_clk_p(mig_ref_clk_clk_p),
        .mig_sys_clk_clk_n(mig_sys_clk_clk_n),
        .mig_sys_clk_clk_p(mig_sys_clk_clk_p),
        .pcie_7x_mgt_0_rxn(pcie_7x_mgt_0_rxn),
        .pcie_7x_mgt_0_rxp(pcie_7x_mgt_0_rxp),
        .pcie_7x_mgt_0_txn(pcie_7x_mgt_0_txn),
        .pcie_7x_mgt_0_txp(pcie_7x_mgt_0_txp),
        .pcie_endpt_reset(pcie_endpt_reset),
        .ref_pcie_clk_clk_n(ref_pcie_clk_clk_n),
        .ref_pcie_clk_clk_p(ref_pcie_clk_clk_p),
        .reset_pb(reset_pb),
        .reset_rtl_0(reset_rtl_0));
endmodule
