##################################################################################################
## 
##  Xilinx, Inc. 2010            www.xilinx.com 
##  Tue Dec  8 14:02:39 2020

##  Generated by MIG Version 4.2
##  
##################################################################################################
##  File name :       example_top.xdc
##  Details :     Constraints file
##                    FPGA Family:       VIRTEX7
##                    FPGA Part:         XC7V2000T-FLG1925
##                    Speedgrade:        -2
##                    Design Entry:      VERILOG
##                    Frequency:         400 MHz
##                    Time Period:       2500 ps
##################################################################################################

##################################################################################################
## Controller 0
## Memory Device: DDR3_SDRAM->SODIMMs->MT8KTF51264HZ-1G9
## Data Width: 64
## Time Period: 2500
## Data Mask: 1
##################################################################################################
############## NET - IOSTANDARD ##################



set_property INTERNAL_VREF  0.900 [get_iobanks 16]
set_property INTERNAL_VREF  0.750 [get_iobanks 40]
set_property INTERNAL_VREF  0.750 [get_iobanks 42]